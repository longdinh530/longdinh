<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<!DOCTYPE html>
<html class="no-js no-svg">
<head>
	<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/tablet.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/mobile.css" />
</head>
<body <?php body_class(); ?>>
<div id="page" class="site">
	<header >
		<div id="navbar">
				<a href="#" >Klubove vyhody</a>
				<a href="#" >na cestach s chinaski</a>
				<a href="#" >Rady &amp; tipy</a>
			<div class="header-left"> 
				<div class="logo-btn" ></div>
			</div>
			<div class="header-right" >
				<div class="arrow-btn" onclick=""><span>PRIHLASENI</span></div>
			</div>
		</div>
		<div class="mobile-navbar header">
			
			 <div class="header-left"> 
				<div class="logo-btn" ></div>
			</div>
			<input class="menu-btn" type="checkbox" id="menu-btn" />
			<label class="menu-icon" for="menu-btn"><span class="navicon"></span>
			<span class="navicon-title">Menu</span></label>
			
			<ul class="menu">
				<li><a href="#work">Our Work</a></li>
				<li><a href="#about">About</a></li>
				<li><a href="#careers">Careers</a></li>
				<li><a href="#contact">Contact</a></li>
			</ul>
		</div>	