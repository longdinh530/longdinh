<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

		<footer id="footer" >
			<div class="left-side">
					<!--logo image -->
					<span class="app-name">©2017 ČPP</span>
					<a href="#" >Úplná pravidla fungování webu</a>
					<a href="#" >Zpracování údajů</a>
			</div> 
			<div class="right-side">
				<div class="company-logo"></div>
			</div>
		</footer>
		</body>
		</html>