<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>


	<div id="header-img" class="header-img">
			<div class="title-section">
				<span class="header-title">Cheete bezstarostne cestovat a ziskat more dalisich vyhod?</span>
				<br/><br/>
				<span class="header-desc">Vstutpe do Klubu cestovatelu CPP </span>
				<br/><br/>
				<div class="nav-signup-btns"> 
					<a href="#" class="btn signup-btn" ><span>E Mailava Adresa</span></a>
					<a href="#" class="btn register-btn"><span>STAT SE CIENEM</span></a><span class="left-arrow" ></span>
				</div>
			</div>
		</div>
		<div id="cookie-message" >
			<div class="cookie-image"></div>
			<div class="cookie-message">
				Po celých stránkách někdo rozsypal sušenky, tak je poctivě sbíráme, abyste pokaždéměli z Klubu cestovatelů ještě lepší zážitek. Přečtěte si více o <span class="red-link">podmínkách používání</span>.
			</div>
			<div class="cookie-button">
				<a href="#" class="btn cookie-confirm-btn">souhlas s cookies</a><span class="left-arrow" ></span>
			</div>
		</div>
		</header><!-- #masthead -->
		<section id="benefits"> 
			<p class="benefits-title" > Klubové výhody </p>
			<p class="benefits-desc" > Pro všechny registrované členy klubu</p>
			<div class="benefit-cards">
				<div class="benefit-card">
					<div class="card-img card-img-1"></div>
					<div class="card-desc">Sleva 25 % na cestovnípojištění online pro každéhoregistrovaného člena</div>
				</div>
				<div class="benefit-card">
					<div class="card-img card-img-2"></div>
					<div class="card-desc">Soutěže</div>				
				</div>
			</div>
		</section>
		<section id="quiz" >
			<div class="quiz-header">
				<span class="title">Klub cesovatelu</span><br>
				<p class="desc">Aktualni soutez pro cleny klubu</p>
			</div>
			<div class="quiz-footer">
				<div class="font-18">
					 Do soutěže se mohou zapojit jen registrovaní členové.Soutěžením <br>souhlasíte s <a href="#" class="red-link font-18">podmínkami soutěže</a></p>.
				</div>
			</div>
			<div class="quiz-popup-box">
				<p>
					5 správných odpovědí a jste v losování o příspěvek 100 EUR na vaši dovolenou
				</p>
				<br/>
				<div class="quiz-btn-container"> 
					<a href="#" class="btn submit-btn"><span>soutěžit</span></a><span class="left-arro" ></span>
				</div>
			</div>
		</section>
		<section id="blog"> 
			<div class="blog-header-img"></div>
			<div class="blog-title ">Australie</div>
			<div class="content">
				<div class="blog-image">

				</div>
				<div class="blog-content">
					Čtrnáct měsíců. Tolik nakonec zabralo natáčení nového alba kapele Chinaski.Novinka s názvem <span class="text-bold">Není nám do pláče</span> vznikala na nejrůznějších místech a česká kapela kvůli ní obletěla zeměkouli – na písničkách pracovala v Austrálii, na Novém Zélandě, ve Velké Británii či v kalifornské poušti.<br/><br/>
					<a href="#" class="hippies" >CESKOAUSTRALSTI HIPPIES </a>
					<hr/>
				</div>
				<div class="ellipsis">
					<div class="ellipsis-item"></div>
					<div class="ellipsis-item"></div>
					<div class="ellipsis-item"></div>
				</div>
				<div class="comment">
					<div class="title">Chcete si přečíst celý článek?</div><br/>
					<div class="desc">Přístup k pravidelným článkům mají pouze členové Klubu cestovatelů.</div>
					<div class="btn-container">
						<a href="#" class="btn gold-btn"><span>Stát se členem</span></a><span class="left-arrow gold-btn-arrow" ></span>
					</div>
					<div class="comment-link ">
						Jste členem klubu? Přihlaste se 
					</div>
				</div>
			</div>
		</section>
		<section id="rady-tipy">
			<div class="title">Rady &amp; tipy </div>
			<div class="content">
					<div class="btn-container">
						<a href="#" class="btn white-btn">Egypt › <span class="font-gold"> Je třeba očkování?</span></a><span class="left-arrow white-btn-arrow" ></span>
					</div>
					<div class="btn-container">
						<a href="#" class="btn white-btn">Chorvatsko › <span class="font-gold" >Jak se vyhnout placení mýta?</span></a><span class="left-arrow white-btn-arrow" ></span>
					</div>
					<div class="btn-container">
						<a href="#" class="btn white-btn">Italské Dolomity ›<span class="font-gold" >Jaké je aktuální počasí?</span></a><span class="left-arrow white-btn-arrow" ></span>
					</div>
			</div>
			<div class="link">
				<a href="#" >Zobrazit vsechny rady &amp; tipy</a>
			</div>
		</section>

	
	


<?php get_footer();
