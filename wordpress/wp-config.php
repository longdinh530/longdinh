<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_database');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         't2nT5L/;&-1)>GtR*0{yim!_Z~PD6jXH[mD=#w.zC7f>pQ==+4amwfmBr<}{QQ4o');
define('SECURE_AUTH_KEY',  '0^{!|cSnY9i]*, 4LI9qMH5Sk{7sbWkF9RuUaYQo*ukZf8qBs!~(nhD*nn>,cWOt');
define('LOGGED_IN_KEY',    'XK</Os3 /f{$ 8}!nFE2%=OjJ2qSZ*5Jd,TmID/SH1;]r<I#dwNFWcWvom*u$IV%');
define('NONCE_KEY',        '-CI;@~ZA9RUc^tb3B<G<m[@EGoIB(cX> :a[z:6y/1*lq=h2DvPCR-E$I9a~o}@~');
define('AUTH_SALT',        '6k}&fC$(AM(zJA$OO=#{bcmJv1SPU`J+#(s4umt/haYfH<^KUz*_O%]{DuBy[oNT');
define('SECURE_AUTH_SALT', 'mo$^Y=,]Xc*VASp3O+ve^2D0~=M8FQx_Mt&L!jpA!D#aF+|lR4W`wPl8%Ra5PJal');
define('LOGGED_IN_SALT',   '+J `uCFz,F6,h_`6ar7M*iYbv}xgc1/YEKC1pQjN`/nD%Iox7j&Y0WTN55a:3J9h');
define('NONCE_SALT',       'sM>J0KtuLcUg)pC.Q._0==?R+)7HYCqZn8up`wA%^Dll/|A`Zp53EwU2fv/u@VK@');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
